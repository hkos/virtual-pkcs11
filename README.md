# Virtual PIV devices

This repository offers container images for easy testing of software that
interacts with PIV hardware tokens - without using actual, physical hardware
tokens.
Instead, these images provide "virtual PIV devices" that can be accessed
in CI environments.

(NOTE: these virtual PIV devices do *not* offer the security benefits of
using hardware security modules! With these images, private key material
is not especially protected.)

Also see: https://gitlab.com/openpgp-card/virtual-cards for container images of
virtual OpenPGP card devices.

## Canokey

[Canokey](https://github.com/canokeys/canokey-core) is an "open-source
security key", which runs multiple applications on one device. It includes
a PIV application.

A Fedora-based image of the Canokey:
`registry.gitlab.com/openpgp-card/virtual-cards/canokey-fedora`

The above container image, plus an installation of current GnuPG and
scdaemon, patched to be able to interact with the Canokey PIV application:
`registry.gitlab.com/hkos/virtual-piv/canokey-gnupg`.

## PivApplet

[PivApplet](https://github.com/arekinath/PivApplet) is a "PIV applet for
JavaCard 2.2.2 and 3.0.4+ with full ECDSA/ECDH support".

A Fedora-Linux based image, including GnuPG and (patched for compatibility)
scdaemon is available as
`registry.gitlab.com/hkos/virtual-piv/pivapplet-gnupg`.

### Using PivApplet with other base images

The top level PivApplet image `registry.gitlab.com/hkos/virtual-piv/pivapplet`
is designed for easy copying into other images, like this (it requires a
Java Runtime Environment (JRE), and the `vsmartcard-vpcd` package):

```
FROM rust

COPY --from=registry.gitlab.com/hkos/virtual-piv/pivapplet /start.sh /
COPY --from=registry.gitlab.com/hkos/virtual-piv/pivapplet /PivApplet /PivApplet/

RUN apt update -y -qq \
 && apt install -y -qq --no-install-recommends \
         openjdk-17-jre vsmartcard-vpcd \
         nettle-dev libclang-dev libpcsclite-dev softhsm2 opensc sq

[..]
```

## OpenFIPS201

https://github.com/makinako/OpenFIPS201

(No images available yet)

# Using these virtual smart cards

With any of these images, you can start the smart card implementation by
running the helper script:

```
sh /start.sh
```

Afterward, the smart card is accessible for use in the container. You can
then run software that uses the virtual PIV card, such as a test-suite.

Example run:

```
$ podman run -it --rm registry.gitlab.com/hkos/virtual-piv/pivapplet-gnupg

[root@773e01b316a8 /]# sh /start.sh > /dev/null
Using reader with a card: Virtual PCD 00 00
Using reader with a card: Virtual PCD 00 00

[root@d99529dba0eb /]# gpg-card
Reader ...........: Virtual PCD 00 00
Serial number ....: FF7F00
Application type .: PIV
Version ..........: 1.0
PIN usage policy .: app-pin
PIN retry counter : - 5 -
PIV authentication: [none]
      keyref .....: PIV.9A
Card authenticat. : [none]
      keyref .....: PIV.9E
Digital signature : [none]
      keyref .....: PIV.9C
Key management ...: [none]
      keyref .....: PIV.9D

gpg/card>
```

# Overview

The stack of software that these PIV images are situated in:

```mermaid
graph BT
    PCSC["pcscd <br/> (PC/SC daemon)"] --> VSC
    VSC["vsmartcard <br/> (Virtual Smart Card Architecture)"] --> JCS["jCardSim <br/> Java Card Runtime Environment Simulator"]
    JCS --> PA["PivApplet"] -.-> TEST["Test suite using virtual PIV card"]
    JCS --> OF201["OpenFIPS201"] -.-> TEST
    PCSC --> CK["Canokey"] -.-> TEST

click VSC "https://frankmorgner.github.io/vsmartcard/virtualsmartcard/README.html"
click JCS "https://jcardsim.org/"

click PA "https://github.com/arekinath/PivApplet"
click OF201 "https://github.com/makinako/OpenFIPS201"
click CK "https://github.com/canokeys/canokey-core"

classDef userApp fill:#f8f8f8,stroke-dasharray: 5 5;
class UA userApp;
```

# Context

These containerized card images were initially developed while exploring
the use of PIV devices in an OpenPGP context:
https://codeberg.org/heiko/pkcs11-openpgp-notes

The virtual PIV cards are used for CI testing in the
[openpgp-piv-sequoia](https://codeberg.org/heiko/openpgp-piv/src/branch/main/lib)
project (e.g. these [integration tests](https://codeberg.org/heiko/openpgp-piv/src/branch/main/cli/ci/canokey)).
