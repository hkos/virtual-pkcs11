# SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
# SPDX-License-Identifier: CC0-1.0

echo "Starting jcardsim" 1>&2
java -noverify -cp APPLET-INF/classes/:/home/jcardsim/jcardsim/target/jcardsim-3.0.5-SNAPSHOT.jar com.licel.jcardsim.remote.VSmartCard jcardsim.cfg &
echo "Started jcardsim" 1>&2

sleep 10

echo "Running opensc-tool" 1>&2
opensc-tool -r 'Virtual PCD 00 00' -s '80 b8 00 00 12 0b a0 00 00 03 08 00 00 10 00 01 00 05 00 00 02 0F 0F 7f'
echo "Done running opensc-tool" 1>&2

