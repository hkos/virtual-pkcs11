# startup convenience script

# Run pcscd (as root)
/etc/init.d/pcscd start

# Run the javacard applet (as user "jcardsim")
su - -c "sh /home/jcardsim/run-card.sh >/dev/null" jcardsim

