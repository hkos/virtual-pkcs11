#!/bin/sh

/etc/init.d/pcscd start
sleep 5

java -noverify -cp /PivApplet/bin/:/PivApplet/jcardsim-3.0.5-SNAPSHOT.jar com.licel.jcardsim.remote.VSmartCard /PivApplet/jcardsim.cfg >/dev/null 2>/dev/null &
sleep 5

opensc-tool --card-driver default --send-apdu 80b80000120ba000000308000010000100050000020F0F7f
opensc-tool -n
