#!/bin/sh

# assume dbus is not running, remove pidfile if it exists from a previous run
mkdir /run/dbus/
rm -f /run/dbus/pid

dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address
pcscd -a -f &
sleep 5

java -noverify -cp /PivApplet/bin/:/PivApplet/jcardsim-3.0.5-SNAPSHOT.jar com.licel.jcardsim.remote.VSmartCard /PivApplet/jcardsim.cfg >/dev/null 2>/dev/null &
sleep 5

opensc-tool --card-driver default --send-apdu 80b80000120ba000000308000010000100050000020F0F7f
opensc-tool -n
